package com.bayes.test.bayestest.database;

import com.bayes.test.bayestest.model.Ability;
import com.bayes.test.bayestest.model.Hero;
import com.mysql.cj.jdbc.Driver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLClient {

    public static final  SQLClient INSTANCE = new SQLClient();

    private JdbcTemplate template;

    private SQLClient() {
        template = getConnection();
    }

    // If the database contains 'hero' table, we can (somewhat) safely assume it contains all the data, TODO "somewhat"
    public boolean containsData() {
        try {
            Connection connection = DriverManager.getConnection(SQLConfig.URL_FULL, SQLConfig.USER, SQLConfig.PASSWORD);
            DatabaseMetaData data = connection.getMetaData();
            ResultSet set = data.getTables(null, null, "hero", null);
            if (set.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    private JdbcTemplate getConnection() {
        SimpleDriverDataSource ds = new SimpleDriverDataSource();
        try {
            ds.setDriver(new Driver());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ds.setUrl(SQLConfig.URL_FULL);
        ds.setUsername(SQLConfig.USER);
        ds.setPassword(SQLConfig.PASSWORD);

        return new JdbcTemplate(ds);
    }

    public void dropTables() {

        String sql = "DROP TABLE IF EXISTS hero";
        template.execute(sql);

        sql = "DROP TABLE IF EXISTS ability";
        template.execute(sql);

        System.out.println("Dropped tables");
    }

    public void createTables() {

        String sql = "CREATE TABLE hero " +
                "(id INTEGER not NULL, " +
                "name TEXT, " +
                "realName TEXT, " +
                "health INTEGER, " +
                "armour INTEGER, " +
                "shield INTEGER, " +
                "description TEXT, " +
                "age INTEGER, " +
                "height INTEGER, " +
                "affiliation TEXT, " +
                "baseOfOperations TEXT, " +
                "difficulty INTEGER, " +
                "url TEXT, " +
                "PRIMARY KEY (id))";
        template.execute(sql);

        sql = "CREATE TABLE ability " +
                "(id INTEGER not NULL, " +
                "name TEXT, " +
                "description TEXT, " +
                "isUltimate TINYINT(1), " +
                "url TEXT, " +
                "heroId INTEGER , " +
                "PRIMARY KEY (id))";
        template.execute(sql);

        System.out.println("Created tables");
    }

    public void insertHeroes(List<Hero> heroes) {
        String columns = "id, name, realName, health, armour, shield, description, age, height, affiliation, baseOfOperations, difficulty, url";
        List<String> values = new ArrayList<>();

        for (Hero hero : heroes) {
            values.add(hero.toSqlValueString());
        }

        batchInsert("hero", columns, values);
    }

    public void insertAbilities(List<Ability> abilities) {
        String columns = "id, name, description, isUltimate, url, heroId";
        List<String> values = new ArrayList<>();

        for (Ability ability: abilities) {
            values.add(ability.toSqlValueString());
        }

        batchInsert("ability", columns, values);
    }

    private void batchInsert(String table, String columns, List<String> values) {
        // TODO: Account for SQL Injection
        template.execute("INSERT INTO " + table + "(" + columns + ") VALUES " + String.join(",", values));
    }

    public List<Hero> getHeroes() {
        return template.query("SELECT * FROM hero", rs -> {
            List<Hero> heroes = new ArrayList<>();
            while (rs.next()) {
                Hero hero = new Hero();
                hero.id = rs.getInt("id");
                hero.name = rs.getString("name");
                hero.realName = rs.getString("realName");
                hero.health = rs.getInt("health");
                hero.armour = rs.getInt("armour");
                hero.shield = rs.getInt("shield");
                hero.description = rs.getString("description");
                hero.age = rs.getInt("age");
                hero.height = rs.getInt("height");
                hero.affiliation = rs.getString("affiliation");
                hero.baseOfOperations = rs.getString("baseOfOperations");
                hero.difficulty = rs.getInt("difficulty");
                hero.url = rs.getString("url");
                heroes.add(hero);
            }
            return heroes;
        });
    }

    public List<Ability> getAbilities() {
        return template.query("SELECT * FROM ability", rs -> {
            List<Ability> abilities = new ArrayList<>();
            while (rs.next()) {
                Ability ability = new Ability();
                ability.id = rs.getInt("id");
                ability.name = rs.getString("name");
                ability.description = rs.getString("description");
                ability.isUltimate = rs.getBoolean("isUltimate");
                ability.url = rs.getString("url");
                ability.hero = new Hero();
                ability.hero.id = rs.getInt("heroId");
                abilities.add(ability);
            }
            return abilities;
        });
    }
}
