package com.bayes.test.bayestest.database;

public class SQLConfig {

    // TODO: These should be in a config file
    //  or at least create a separate SQLConfig-example.java file and don't keep this file in the repository
    private static final String HOST =     "<your-server-host";
    private static final String PORT =     "<your-database-port>";
    private static final String NAME =     "<your-database-name";
    private static final String PARAMS =   "<your-db-params>";

    public static final String URL_FULL = "jdbc:mysql://" + HOST + ":" + PORT + "/" + NAME + "?" + PARAMS;
    public static final String USER =     "<your-database-user";
    public static final String PASSWORD = "<your-database-password>";

    public static boolean isSet() {
        return HOST != "<your-server-host";
    }
}
