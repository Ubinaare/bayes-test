package com.bayes.test.bayestest.utils;

public class TextUtils {

    public static String escapeApostrophe(String input) {
        return input != null ? input.replace("'", "\\'") : null;
    }
}
