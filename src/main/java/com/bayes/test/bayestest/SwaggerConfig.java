package com.bayes.test.bayestest;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                // https://stackoverflow.com/questions/33431343/avoiding-default-basic-error-controller-from-swagger-api
                .paths(Predicates.not(PathSelectors.regex("/error.*")))//<6>, regex must be in double quotes.
                .build().apiInfo(apiInfo());
    }

    ApiInfo apiInfo() {
        List<VendorExtension> vendorExtensions = new ArrayList<>();
        return new ApiInfo(
                "Overwatch API",
                "Returns hero and ability info",
                "1.0.0",
                null,
                new Contact("Aare Undo", "", "aare.undo@gmail.com"),
                null,
                null, vendorExtensions);
    }
}
