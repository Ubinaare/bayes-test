package com.bayes.test.bayestest;

import com.bayes.test.bayestest.database.SQLClient;
import com.bayes.test.bayestest.database.SQLConfig;
import com.bayes.test.bayestest.model.Ability;
import com.bayes.test.bayestest.model.Hero;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class BayestestApplication {

    private static final String BASE_URL = "https://overwatch-api.net/api/v1/";
    private static final String ABILITY_URL = BASE_URL + "ability?page=1";

    public static final List<Hero> HEROES= new ArrayList<>();
    public static final List<Ability> ABILITIES = new ArrayList<>();

    public static void main(String[] args) throws Exception {

        // Java user agent is forbidden, mock browser
        String agent = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2";
        System.setProperty("http.agent", agent);

        SpringApplication.run(BayestestApplication.class, args);

        if (!SQLConfig.isSet()) {
            throw new Exception("SQL configuration not set in SQLConfig.java");
        }

        // TODO: Any damn basic ErrorHandling
        // Also, in reality, there should be a separate service for filling the database
        if (SQLClient.INSTANCE.containsData()) {
            HEROES.addAll(SQLClient.INSTANCE.getHeroes());
            ABILITIES.addAll(SQLClient.INSTANCE.getAbilities());

        } else {
            SQLClient.INSTANCE.createTables();

            ObjectMapper mapper = new ObjectMapper();
            mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

            // Recursively request abilities, while mapping hero data under abilities,
            // because this results in the least amount of total queries
            requestAbilities(ABILITY_URL, mapper);

            addUniqueHeroes();

            SQLClient.INSTANCE.insertHeroes(HEROES);
            SQLClient.INSTANCE.insertAbilities(ABILITIES);
        }
    }

    private static void addUniqueHeroes() {
        for (Ability ability: ABILITIES) {
            if (HEROES.stream().noneMatch(o -> o.id == ability.hero.id)) {
                HEROES.add(ability.hero);
            }
        }
    }

    private static void requestAbilities(String url, ObjectMapper mapper) {
        try {
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            String json = content.toString();
            in.close();

            JsonNode node = mapper.readTree(json);
            String data = node.get("data").toString();
            List<Ability> abilities = mapper.readValue(data, new TypeReference<List<Ability>>(){ });

            ABILITIES.addAll(abilities);

            if (!node.get("next").isNull()) {
                url = node.get("next").toString();
                // The urls provided by Overwatch api are http, need to be https; also, remove double-quotes
                url = url.replace("http", "https").replace("\"", "");
                requestAbilities(url, mapper);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
