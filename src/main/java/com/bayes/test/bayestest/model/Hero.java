package com.bayes.test.bayestest.model;

import com.bayes.test.bayestest.utils.TextUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class Hero {

    // Ignoring java getter/setter conventions for brevity's & time's sake

    @JsonProperty
    public int id;

    @JsonProperty
    public String name;

    @JsonProperty
    public String realName;

    @JsonProperty
    public int health;

    @JsonProperty
    public int armour;

    @JsonProperty
    public int shield;

    @JsonProperty
    public String description;

    @JsonProperty
    public int age;

    @JsonProperty
    public Integer height;

    @JsonProperty
    public String affiliation;

    @JsonProperty
    public String baseOfOperations;

    @JsonProperty
    public Integer difficulty;

    @JsonProperty
    public String url;

    public String toSqlValueString() {
        // TODO properly serialize hero to sql values
        return  "(" + id + ", '" + TextUtils.escapeApostrophe(name) + "', '" + TextUtils.escapeApostrophe(realName) + "', " +
                health + ", " + armour + ", " + shield + ", '" + TextUtils.escapeApostrophe(description) + "', " +
                age + ", " + height + ", '" + TextUtils.escapeApostrophe(affiliation) + "', '" +
                TextUtils.escapeApostrophe(baseOfOperations) + "', " + difficulty + ", '" + url + "')";
    }
}
