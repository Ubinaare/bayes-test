package com.bayes.test.bayestest.model;

import com.bayes.test.bayestest.utils.TextUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class Ability {

    // Ignoring java getter/setter conventions for brevity's & time's sake
    // Defined all properties, not just the required ones, for json serialization purposes
    // TODO: Base class for Hero & Ability to share common properties

    @JsonProperty
    public int id;

    @JsonProperty
    public String name;

    @JsonProperty
    public String description;

    @JsonProperty
    public boolean isUltimate;

    @JsonProperty
    public String url;

    // TODO: Should be under @JsonIgnore for local api, but needs to be @JsonProperty for overwatch-api.net json serialization
    @JsonProperty
    public Hero hero;

    public String toSqlValueString() {
        // TODO properly serialize ability to sql values
        return "(" + id + ", '" + TextUtils.escapeApostrophe(name) + "', '" +
                TextUtils.escapeApostrophe(description) + "', " + isUltimate + ", '" + url + "', " + hero.id + ")";
    }
}
