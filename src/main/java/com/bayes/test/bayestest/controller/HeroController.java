package com.bayes.test.bayestest.controller;

import com.bayes.test.bayestest.BayestestApplication;
import com.bayes.test.bayestest.model.Ability;
import com.bayes.test.bayestest.model.Hero;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@Api(value = "HeroController", tags = { "Heroes" })
@RestController
class HeroController {

    @ApiOperation(value = "Get list of Heroes in the System")
    @RequestMapping(value = "/api/heroes", method = RequestMethod.GET)
    List<Hero> getHeroes() {
        return BayestestApplication.HEROES;
    }

    @ApiOperation(value = "Get hero based on its {id} parameter")
    @RequestMapping(value = "/api/heroes/{id}", method = RequestMethod.GET)
    Hero getHero(@PathVariable int id) throws Exception {
        List<Hero> heroes = BayestestApplication.HEROES.stream().filter(h -> h.id == id).collect(Collectors.toList());

        if (heroes.size() == 0) {
            throw new Exception("No hero with such an Id found");
        }

        return heroes.get(0);
    }

    @ApiOperation(value = "Get the abilities of a hereo based on {id} parameter")
    @RequestMapping(value = "/api/heroes/{id}/abilities", method = RequestMethod.GET)
    List<Ability> getAbilities(@PathVariable int id) {
        return BayestestApplication.ABILITIES.stream().filter(h -> h.hero.id == id).collect(Collectors.toList());
    }
}
