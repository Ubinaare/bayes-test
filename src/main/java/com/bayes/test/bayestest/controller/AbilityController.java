package com.bayes.test.bayestest.controller;

import com.bayes.test.bayestest.BayestestApplication;
import com.bayes.test.bayestest.model.Ability;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@Api(value = "AbilityController", tags = { "Abilities" })
@RestController
public class AbilityController {

    @ApiOperation(value = "Get all abilities of all heroes")
    @RequestMapping(value = "/api/abilities", method = RequestMethod.GET)
    List<Ability> getAbilities() {
        return BayestestApplication.ABILITIES;
    }

    @ApiOperation(value = "Get an ability based on based on its {id} parameter")
    @RequestMapping(value = "/api/abilities/{id}", method = RequestMethod.GET)
    Ability getAbility(@PathVariable int id) throws Exception {
        List<Ability> abilities = BayestestApplication.ABILITIES.stream().filter(h -> h.id == id).collect(Collectors.toList());

        if (abilities.size() == 0) {
            throw new Exception("No ability with such an id found");
        }

        return abilities.get(0);
    }
}
