## Bayes eSports test assignment

### Setup

Project created with Intellij IDEA Ultimate edition and [Spring Initializr](https://start.spring.io/). Uses the following versions:

* `JDK 1.8`
* `gradle 5.4.1`
* Worked with `10.2.25-MariaDB` database

All other dependencies defined in `build.gradle` and it should work out of the box

Runs on the default port: `8080`

Be sure to set database configuration in `src/main/java/com/bayes/test/bayestest/database/SQLConfig.java`

### Spring security

Spring Security is a powerful and highly customizable authentication and access-control framework. It is the de-facto standard for securing Spring-based applications. [[1]](https://spring.io/projects/spring-security)

In short, you need to enter a password with each session development session:

* **username**: user
* **password**: generated on build, e.g. `Using generated security password: 971a95a0-7406-4952-bfc4-4214ad8b5bfa`

### API Doc

Documentation generated with [Springfox](https://springfox.github.io/springfox/) and [Swagger UI](https://swagger.io/tools/swagger-ui/)

Available at: `/swagger-ui.html`

### Questions and Comments

Most of what I didn't manage to achieve in the required timeframe is defined via `TODO` tags in the project. 

However, I would like to point out that, as **sloppy** as the code is, I already went a bit over the suggested time limit and didn't get anywhere near writing the actual tests.

Finally, I would have personally written the project using Kotlin. Seems like a new and fun thing to work with. It isn't going anywhere either, since it's now a first-level language for Android, supported by Google. What do you guys think of Kotlin?

